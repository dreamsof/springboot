package com.dove.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @ProjectName: websocket
 * @Package: com.dove.listener
 * @ClassName: ${TYPE_NAME}
 * @Description: java类作用描述
 * @Author: dove
 * @CreateDate: 2018/7/22 21:01
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
@Component //它的主要作用就是将这个监听器纳入到Spring容器中进行管理
public class RequestListener implements ServletRequestListener {
    private static final Logger log = LoggerFactory.getLogger(RequestListener.class);
    @Override
    public void requestInitialized(ServletRequestEvent sre)  {
        //将所有request请求都携带上httpSession
        HttpSession session = ((HttpServletRequest) sre.getServletRequest()).getSession();
    }
    public RequestListener() {}

    @Override
    public void requestDestroyed(ServletRequestEvent arg0)  {}
}