package com.dove.constant;

/**
 * @ProjectName: websocket
 * @Package: com.dove.constant
 * @ClassName: Constants
 * @Description: 常量类
 * @Author: dove
 * @CreateDate: 2018/7/22 22:14
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
public class Constants {
    public static final String USER = "user";
}
