package com.dove.domain;

/**
 * @ProjectName: websocket
 * @Package: com.dove.domain
 * @ClassName: User
 * @Description: User实体类
 * @Author: dove
 * @CreateDate: 2018/7/22 22:11
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
public class User {
    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                '}';
    }
}
