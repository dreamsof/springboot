<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>应用首页</title>
	<#include "/resources.ftl">
</head>

<body>
	<div id="wrapper">
		<!-- 主页面 -->
		<div id="page-wrapper">
			<div class="row">
				<div class="col-lg-12">
					<!--<ol class="breadcrumb">
					  <li>首页/</li>
					</ol>-->
					<h1 class="page-header">
						<span>欢迎您<#if  Session.user?exists>
					，${Session.user.username}!
					</#if></span>
					</h1>
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
			<div class="row">
				<div class="col-lg-3 col-md-6">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<div class="row">
								<div class="col-xs-3">
									<i class="fa fa-comments fa-5x"></i>
								</div>
								<div class="col-xs-9 text-right">
									<div>excel导入</div>
								</div>
							</div>
						</div>
						<a href="#">
							<div class="panel-footer">
								<span class="pull-left">
									<form id="uploadForm" target="frameFile"  method="post" action="/user/fileUpLoad" enctype="multipart/form-data">
										<input id="files" name="files" type="file" style="width:200px;"/>
									</form>
								</span> 
								<span class="pull-right"><i class="fa fa-arrow-circle-right">
									
								</i></span>
								<div class="clearfix"></div>
							</div>
						</a>
					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="panel panel-success">
						<div class="panel-heading">
							<div class="row">
								<div class="col-xs-3">
									<i class="fa fa-tasks fa-5x"></i>
								</div>
								<div class="col-xs-9 text-right">
									<div class="huge">12</div>
									<div>New Tasks!</div>
								</div>
							</div>
						</div>
						<a href="#">
							<div class="panel-footer">
								<span class="pull-left">View Details</span> 
								<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
								<div class="clearfix"></div>
							</div>
						</a>
					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="panel panel-info">
						<div class="panel-heading">
							<div class="row">
								<div class="col-xs-3">
									<i class="fa fa-shopping-cart fa-5x"></i>
								</div>
								<div class="col-xs-9 text-right">
									<div class="huge">124</div>
									<div>New Orders!</div>
								</div>
							</div>
						</div>
						<a href="#">
							<div class="panel-footer">
								<span class="pull-left">View Details</span> 
								<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
								<div class="clearfix"></div>
							</div>
						</a>
					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="panel panel-warning">
						<div class="panel-heading">
							<div class="row">
								<div class="col-xs-3">
									<i class="fa fa-support fa-5x"></i>
								</div>
								<div class="col-xs-9 text-right">
									<div class="huge">13</div>
									<div>Support Tickets!</div>
								</div>
							</div>
						</div>
						<a href="#">
							<div class="panel-footer">
								<span class="pull-left">View Details</span> 
								<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
								<div class="clearfix"></div>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
		<!-- /#page-wrapper -->
		<nav class="navbar navbar-default">
			<div class="navbar-brand">日志</div>
		</nav>
		<!-- <div class="alert alert-success" role="alert">
	        <strong>Well done!</strong> You successfully read this important alert message.
        </div>
        <div class="alert alert-info" role="alert">
        	<strong>Heads up!</strong> This alert needs your attention, but it's not super important.
        </div>
        <div class="alert alert-warning" role="alert">
        	<strong>Warning!</strong> Best check yo self, you're not looking too good.
        </div>
        <div class="alert alert-danger" role="alert">
        	<strong>Oh snap!</strong> Change a few things up and try submitting again.
        </div> -->
		<div id="log-container" style="height: 390px; overflow-y: scroll; color: #aaa; padding: 10px;">
	        <div id="logs"></div>
   		</div>
		<!-- <div class="progress">
        	<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"><span class="sr-only">40% Complete (success)</span></div>
      	</div> -->
	</div>
	
</body>
	<script src="/js/sockjs.min.js"></script>
    <script src="/js/stomp.min.js"></script>
	<script src="/js/sockt.log.js"></script>
    <script src="/js/index.js"></script>
</html>