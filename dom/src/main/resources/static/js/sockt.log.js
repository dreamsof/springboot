var websocket = null;

    //判断当前浏览器是否支持WebSocket
    if('WebSocket' in window){
        websocket = new WebSocket("ws://"+window.location.host+"/websocket");
    }
    else{
        alert('Not support websocket')
    }

    //连接发生错误的回调方法
    websocket.onerror = function(){
        setMessageInnerHTML("error");
    };

    //连接成功建立的回调方法
    websocket.onopen = function(event){
        setMessageInnerHTML("open");
    }

    //接收到消息的回调方法
    websocket.onmessage = function(event){
        setMessageInnerHTML(event.data);
    }

    //连接关闭的回调方法
    websocket.onclose = function(){
        setMessageInnerHTML("close");
    }

    //监听窗口关闭事件，当窗口关闭时，主动去关闭websocket连接，防止连接还没断开就关闭窗口，server端会抛异常。
    window.onbeforeunload = function(){
        websocket.close();
    }

    //将消息显示在网页上
    function setMessageInnerHTML(message){
//        $("#log-container div").append(message + "<p> </p>");
//        $("#log-container").scrollTop($("#log-container div").height() - $("#log-container").height());
    	
   	  // 接收服务端的实时日志并添加到HTML页面中
      $("#log-container #logs").append("<div class='alert alert-success'>" + message + "</div><p> </p>");
      // 滚动条滚动到最低部
      $("#log-container").scrollTop($("#log-container #logs").height() - $("#log-container").height());
    }

    //关闭连接
    function closeWebSocket(){
        websocket.close();
    }


//var stompClient = null;
//
//function connect(){
//    var socket = new SockJS('/endpointAric'); //连接SockJS的endpoint名称为"endpointWisely"
//    stompClient = Stomp.over(socket);//使用STMOP子协议的WebSocket客户端
//    stompClient.connect({},function(frame){//连接WebSocket服务端
//        console.log('Connected:' + frame);
//        //通过stompClient.subscribe订阅/topic/getResponse 目标(destination)发送的消息,这个是在控制器的@SentTo中定义的
//        stompClient.subscribe('/topic/sendLogMsg',function(response){
//        	console.log('==============================================================:');
//            showResponse(JSON.parse(response.body).responseMessage);
//        });
//    });
//}
//
//function disconnect(){
//    if(stompClient != null) {
//        stompClient.disconnect();
//    }
//    console.log("Disconnected");
//}
//
//function showResponse(message){
//	 // 接收服务端的实时日志并添加到HTML页面中
//    $("#log-container div").append(message + "<p> </p>");
//    // 滚动条滚动到最低部
//    $("#log-container").scrollTop($("#log-container div").height() - $("#log-container").height());
//}