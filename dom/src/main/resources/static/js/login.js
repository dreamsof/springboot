function login(){
	var data = $('#form').serialize();
	$.ajax({
		type:"get",
		url:"/user/index",
		data:data,
		success:function(msg){
			if("success" == msg) {
				layer.msg('登录成功');
				window.location.href="/user/doLogin";
			}else{
				layer.msg('账号或者密码输入错误！');
			}
		}
		
	});
}

























//$(document).ready(function() {
//    $('#registerUserForm').bootstrapValidator({
//        message: 'This value is not valid',
//        feedbackIcons: {
//            valid: 'glyphicon glyphicon-ok',
//            invalid: 'glyphicon glyphicon-remove',
//            validating: 'glyphicon glyphicon-refresh'
//        },
//        fields: {
//        	loginName: {
//                validators: {
//                    notEmpty: {
//                        message: '登录名不能为空'
//                    },
//                    stringLength: {
//                        min: 4,
//                        max: 18,
//                        message: '登录名长度必须在4到18位之间'
//                    },
//                    regexp: {
//                        regexp: /^[\u4e00-\u9fa5_a-zA-Z0-9]+$/,
//                        message: '登录名只能包含中文,英文字母和数字及下划线'
//                    },
//                    callback: {
//                        message: '登录名称已经存在，不能重复添加！',
//                        callback: function(value, validator) {
//                            //return value != 'ww';
//                        	var flag = false;
//                    	    $.ajax({
//                    			type:"post",
//                    			async:false,  //异步
//                    			url:"user/queryUserByAjax.do",
//                    			data:{
//                    				"loginName":value
//                    			},
//                    			success:function(msg){
//                    				if(msg=="null"){
//                    					flag = true;
//                    				}else{
//                    					flag = false;
//                    				}
//                    			},
//                    			error:function(msg){
//                    				flag = false;
//                    			}
//                    		}); 
//                    	    return flag ;
//                        }
//                    }
//                }
//            },
//            userName: {
//                message: '用户名验证失败',
//                validators: {
//                    notEmpty: {
//                        message: '用户名不能为空'
//                    },
//                    stringLength: {
//                        min: 2,
//                        max: 18,
//                        message: '用户名长度必须在2到18位之间'
//                    },
//                    regexp: {
//                        regexp: /^[\u4e00-\u9fa5_a-zA-Z0-9]+$/,
//                        message: '用户名只能包含中文,英文字母和数字及下划线'
//                    }
//                }
//            },
//            email: {
//                validators: {
//                    notEmpty: {
//                        message: '邮箱不能为空'
//                    },
//                    emailAddress: {
//                        message: '邮箱地址格式有误'
//                    }
//                }
//            },
//            phone: {
//                validators: {
//                    notEmpty: {
//                        message: '手机号不能为空'
//                    },
//                    stringLength: {
//                        min: 11,
//                        max: 11,
//                        message: '手机号长度为11位'
//                    },
//                    regexp: {
//                        regexp: /^[0-9]+$/,
//                        message: '手机号只能是数字'
//                    }
//                }
//            },
//            gender: {
//                validators: {
//                    notEmpty: {
//                        message: '性别不能为空'
//                    }
//                   
//                }
//            },
//            roleName: {
//                validators: {
//                    notEmpty: {
//                        message: '角色不能为空'
//                    }
//                   
//                }
//            },
//         
//        }
//    });
//});
