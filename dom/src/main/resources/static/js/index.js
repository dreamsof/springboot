/*function openFileWindow(){
	layer.open({
		  type: 2,
		  area: ['600px', '530px'],
		  maxmin: false,
		  title:'选择要上传的文件',
		  content: 'fileUpload.html'
	});
}*/
$("#files").on("change", function(){
	var filePath = $("#files").val();
	if (!filePath.toLocaleLowerCase().endsWith(".xls")&&!filePath.toLocaleLowerCase().endsWith(".xlsx")) {
		layer.msg("请上传后缀名为.xls或者.xlsx的文件");
        return false;
	}
	$("#uploadForm").submit();
});