package com.th.dom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement//开启事务管理
@EnableScheduling//增加支持定时任务的注解
public class DomApplication {

    public static void main(String[] args) {
        SpringApplication.run(DomApplication.class, args);
    }
}
