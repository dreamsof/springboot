package com.th.dom.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.th.dom.dao.UserLogMapper;
import com.th.dom.domain.UserLog;
import com.th.dom.service.UserLogService;
@Service
public class UserLogServiceImpl implements UserLogService {

	@Autowired
	private UserLogMapper userLogMapper;
	@Override
	public int insertUserLog(UserLog userLog) {
		
		return userLogMapper.insertUserLog(userLog);
	}

}
