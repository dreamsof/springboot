package com.th.dom.service;


import com.th.dom.domain.T_User;
import com.th.dom.domain.User;
/**
 * 

* <p>Title: UserService</p>  

* <p>Description: 用户业务层</p>  

* @author dove  

* @date 2018年7月20日
 */
public interface UserService {
    /**
     *  根据用户名称查询用户信息
     * @param name
     * @return
     */
    User selectUserByName(String name);
    
    int insert(User User);
}

