package com.th.dom.service;


import com.th.dom.domain.UserLog;
/**
 * 

* <p>Title: UserLogService</p>  

* <p>Description: 用户日志业务层</p>  

* @author dove  

* @date 2018年7月20日
 */
public interface UserLogService {
	/**
	 * 
	  * @Description: 插入一条用户日志信息
	  * @author : dove
	  * @param userLog
	  * @return 
	  * @date  2018年7月20日
	 */
	int insertUserLog(UserLog userLog);
}

