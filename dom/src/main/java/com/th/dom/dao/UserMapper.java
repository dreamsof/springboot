package com.th.dom.dao;

import com.th.dom.domain.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @Package:        com.th.dom.dao
 * @ClassName:      UserMapper
 * @Description:    用户mapper
 * @Author:         dove
 * @CreateDate:     2018/7/17 15:20
 * @Version:        1.0
 * <p>Copyright: Copyright (c) 2018/7/17</p>
 *
 */
@Mapper  //声明是一个Mapper,与springbootApplication中的@MapperScan二选一写上即可
@Repository
public interface UserMapper {

    User selectUserByName(String name);
    int insert(User user);
}
