package com.th.dom.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

/**
 * 

* <p>Title: WebSecurityConfig</p>  

* <p>Description: Spring Security配置</p>  

* @author dove  

* @date 2018年7月21日下午2:47:25
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter{

    @Override
    protected void configure(HttpSecurity http) throws Exception {
    	http.csrf().disable();  ///关闭防跨域攻击功能 解决无法post提交数据问题
        http.authorizeRequests()
//        	.antMatchers("/resources/**", "/webjars/**","/js/**").permitAll()
        	.antMatchers("/**/*.css","/**/*.js","/**/*.map").permitAll()
//            .antMatchers("").permitAll() //设置Spring Security对/和"/login"路径不拦截
            .antMatchers("/favicon.ico").permitAll()
//            .anyRequest().authenticated()
            .and()
            .formLogin().loginPage("/user/login")//设置Spring Security的登陆页面访问路径为login
            .defaultSuccessUrl("/user") //登陆成功后转向/chat路径
            .permitAll().and()
//            .rememberMe().tokenValiditySeconds(1209600).key("myKey").and()
            .logout()
//             .logoutRequestMatcher(new AntPathRequestMatcher("/loginOut"))
//            .logoutSuccessUrl("/")
//            .invalidateHttpSession(true) //是否清除Http session中的内容
            .permitAll();
        //以下这句就可以控制单个用户只能创建一个session，也就只能在服务器登录一次        
        http.sessionManagement().maximumSessions(1).expiredUrl("/user/login");
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        //配置两个用户,角色是user
        auth.inMemoryAuthentication()
            .withUser("zs").password("1").roles("user")
            .and()
            .withUser("curry").password("curry").roles("user");
//        auth.userDetailsService(userDetailsService);
    }
    //放在.antMatchers("/resources/**", "/webjars/**", "/img/**").permitAll()
    /*@Override
    public void configure(WebSecurity web) throws Exception {
        // 设置Spring Secutiry不拦截/resources/static/目录下的静态资源
        web.ignoring().antMatchers("/js/**");
    }*/

}