package com.th.dom.ext;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class PoiReadExcel07 {
	public static void main(String[] args) {
		File file = new File("D:/123.xlsx");
		try {
			XSSFWorkbook workbook = new XSSFWorkbook(FileUtils.openInputStream(file));
			Sheet sheet = workbook.getSheetAt(0);
			int firstRowNum = 0;//从第n行开始读取
			int lastRowNum = sheet.getLastRowNum();
			for (int i = firstRowNum; i < lastRowNum; i++) {
				Row row = sheet.getRow(i);
				int lastCellNum = row.getLastCellNum();
				for (int j = 0; j < lastCellNum; j++) {
					Cell cell = row.getCell(j);
					String value = cell.getStringCellValue();
					System.out.print(value+" ");
				}
				System.out.println();
			}
			workbook.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
