package com.th.dom.ext;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class PoiExpExcel {
	public static void main(String[] args) {
		String[] title={"id","name","sex"};
		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet();
		HSSFRow row = sheet.createRow(0);//title
		HSSFCell cell = null;
		for (int i = 0; i < title.length; i++) {
			cell = row.createCell(i);
			cell.setCellValue(title[i]);
		}
		HSSFRow nextrow = null;
		for (int i = 1; i < 10; i++) {
			nextrow = sheet.createRow(i);
//			for (int j = 0; j < title.length; j++) {
//				cell = nextrow.createCell(j);
//				cell.setCellValue("a");
//			}
			cell = nextrow.createCell(0);
			cell.setCellValue("a"+i);
			cell = nextrow.createCell(1);
			cell.setCellValue("user"+i);
			cell = nextrow.createCell(2);
			cell.setCellValue("男"+i);
			
		}
		File file = new File("D:/12.xls");
		try {
			file.createNewFile();
			FileOutputStream stream = FileUtils.openOutputStream(file);
			workbook.write(stream);
			stream.close();
			workbook.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
}
