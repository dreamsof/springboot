package com.th.dom.ext;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class PoiExpExcel07 {
	public static void main(String[] args) {
		String[] title={"id","name","sex"};
		XSSFWorkbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet();
		Row row = sheet.createRow(0);//title
		Cell cell = null;
		for (int i = 0; i < title.length; i++) {
			cell = row.createCell(i);
			cell.setCellValue(title[i]);
		}
		Row nextrow = null;
		for (int i = 1; i < 10; i++) {
			nextrow = sheet.createRow(i);
//			for (int j = 0; j < title.length; j++) {
//				cell = nextrow.createCell(j);
//				cell.setCellValue("a");
//			}
			cell = nextrow.createCell(0);
			cell.setCellValue("a"+i);
			cell = nextrow.createCell(1);
			cell.setCellValue("user"+i);
			cell = nextrow.createCell(2);
			cell.setCellValue("男"+i);
			
		}
		File file = new File("D:/123.xlsx");
		try {
			file.createNewFile();
			FileOutputStream stream = FileUtils.openOutputStream(file);
			workbook.write(stream);
			stream.close();
			workbook.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
}
