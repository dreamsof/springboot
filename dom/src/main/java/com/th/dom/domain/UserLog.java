package com.th.dom.domain;

public class UserLog {
	 private String id;
	 private String username;
	 private String login_ip;
	 private Integer type;
	 private String operation;
	 private String remark;
	 private String computer;
	 
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getLogin_ip() {
		return login_ip;
	}
	public void setLogin_ip(String login_ip) {
		this.login_ip = login_ip;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	public String getComputer() {
		return computer;
	}
	public void setComputer(String computer) {
		this.computer = computer;
	}
	@Override
	public String toString() {
		return "UserLog [id=" + id + ", username=" + username + ", login_ip="
				+ login_ip + ", type=" + type + ", operation=" + operation
				+ ", remark=" + remark + ", computer=" + computer + "]";
	}
	
	 
}
