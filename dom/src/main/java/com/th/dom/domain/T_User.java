package com.th.dom.domain;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
/**
  * 加载yaml配置文件的方法
  * spring-boot更新到1.5.2版本后locations属性无法使用
  * @PropertySource注解只可以加载proprties文件,无法加载yaml文件
  * 故     1.现在把数据放到application.yml文件中,spring-boot启动时会加载
  * 	2.放在新建的properties文件中
  */
/**
 * 

* <p>Title: T_User</p>  

* <p>Description: 用户日志bean</p>  

* @author dove  

* @date 2018年7月21日上午10:27:45
 */
@Component
@ConfigurationProperties(prefix="T_User")
@PropertySource("classpath:cofig/table.properties")
public class T_User {
	private String id;
    private String username;
    private String age;
    private String customerid;
    private String password;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getCustomerid() {
		return customerid;
	}
	public void setCustomerid(String customerid) {
		this.customerid = customerid;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
    
}
