//package com.th.dom.schedules;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Component;
//
///**
//     * @Package:        com.th.dom.schedules
//     * @ClassName:      SchedulingTest
//     * @Description:    定时器
//     * @Author:         dove
//     * @CreateDate:     2018/7/17 19:47
//     * @Version:        1.0
//     * <p>Copyright: Copyright (c) 2018/7/17</p>
//     *
//     */
//@Component
//public class SchedulingTest {
//    private final Logger logger = LoggerFactory.getLogger(SchedulingTest.class);
//
//    // 每5秒执行一次
//    @Scheduled(cron = "0/5 * * * * ?")
//    protected void scheduler() {
//        logger.info(">>>>>>>>>>>>> scheduled test... ");
//    }
//
//}
